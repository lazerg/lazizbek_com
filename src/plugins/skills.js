import { snakeCase } from "lodash";

const $ = {
    skills: [],

    add: (name, sub_skills) => ({
        name,
        sub_skills,
        photo: 'assets/skills/' + snakeCase(name) + '.jpg',
        search: 'https://google.com/search?q=' + name,
    }),

    addSkill (name, sub_skills) {
        this.skills.push(this.add(name, sub_skills));
    },

}

$.addSkill('Backend development', [
    $.add('PHP'),
    $.add('Laravel'),
    $.add('Redis'),
    $.add('NodeJS'),
    $.add('RestAPI'),
    $.add('GraphQL'),
])

$.addSkill('Frontend development', [
    $.add('HTML'),
    $.add('CSS'),
    $.add('JavaScript'),
    $.add('VueJS'),
    $.add('VuetifyJS'),
    $.add('Material Design')
])

$.addSkill('DevOps', [
    $.add('Docker'),
    $.add('Nginx'),
    $.add('Apache'),
    $.add('GitLab'),
])

$.addSkill('Database development', [
    $.add('MySQL'),
    $.add('PostgreSQL'),
])

export default $.skills;
