import { snakeCase } from "lodash";

const $ = {
    projects: [],

    addProject (name, links) {
        this.projects.push({
            name,
            photo: 'projects/' + snakeCase(name) + '.jpg',
            links,
        });
    },

    addLink: (name, link) => ({
        name,
        photo: 'projects/links/' + snakeCase(name) + '.jpg',
        link,
    }),
}

$.addProject('Robocontest', [
    $.addLink('Visit', 'https://robocontest.uz/'),
]);

$.addProject('Rapid Framework', [
    $.addLink('GitLab', 'https://gitlab.com/lazerg/rapid/'),
]);

$.addProject('Medianar.ru', [
    $.addLink('Visit', 'https://medianar.ru/'),
]);

$.addProject('Aleshin`s Workshop', [
    $.addLink('Visit', 'https://oldrusuvenir.ru/'),
]);

export default $.projects;
