import { snakeCase } from "lodash";

const $ = {
    contacts: [],

    addContact (name, link) {
        this.contacts.push({
            name,
            photo: 'assets/contacts/' + snakeCase(name) + '.jpg',
            link,
        });
    },
}

$.addContact('Freelancehunt', 'https://freelancehunt.com/freelancer/lazerg.html?r=a3Gnv');
$.addContact('Gmail', 'mailto:lazerg2@gmail.com');
$.addContact('Linkedin', 'https://www.linkedin.com/in/lazizbek-ergashev-81a48412a/');
$.addContact('Telegram', 'https://t.me/lazizbek_ergashev');
$.addContact('Instagram', 'https://instagram.com/_lazerg_');

export default $.contacts;

