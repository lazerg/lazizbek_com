import Vue from 'vue';
import VueRouter from "vue-router";

Vue.use(VueRouter);

export default new VueRouter({
    mode: 'history',

    routes: [
        {
            component: /* webpackChunkName: "home-page" */ () => import('@/views/Home'),
            name: 'home',
            path: '/home',
        },

        {
            component: /* webpackChunkName: "home-page" */ () => import('@/views/Portfolio'),
            name: 'portfolio',
            path: '/portfolio'
        },

        {
            component: /* webpackChunkName: "skills-page" */ () => import('@/views/Skills'),
            name: 'skills',
            path: '/skills',
        },

        {
            component: /* webpackChunkName: "home-page" */ () => import('@/views/Contacts'),
            name: 'contacts',
            path: '/contacts',
        },

        {
            path: '/',
            redirect: '/home',
        }
    ]
})
