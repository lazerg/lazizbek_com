import Vue from "vue";
import Vuex from "vuex";
import vuetify from "@/plugins/vuetify";
import skills from "@/plugins/skills";
import contacts from "@/plugins/contacts";
import projects from "@/plugins/projects";

Vue.use(Vuex);

export default new Vuex.Store({
    state: () => ({
        app_name: "lazergDev",
        app_version: "v0.2.2",
        drawer: false,
        links: [
            { name: "Home", icon: "mdi-home" },
            { name: "Portfolio", icon: "mdi-folder" },
            { name: "Skills", icon: "mdi-head-flash" },
            { name: "Contacts", icon: "mdi-email" },
        ],
        skills,
        contacts,
        projects,
    }),

    getters: {
        isMobile: () => vuetify.framework.breakpoint.mdAndDown,
        isDesktop: () => vuetify.framework.breakpoint.lgAndUp,

        onlyLinks: state => linkName => {
            return state.links.filter(link => linkName.includes(link.name));
        },
    },

    mutations: {
        SET_DRAWER (state, payload) {
            state.drawer = payload;
        },
    },
})
