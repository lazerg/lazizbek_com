import Vue from "vue";
import App from "./App.vue";
import vuetify from "./plugins/vuetify";
import router from "@/plugins/router";
import store from "@/plugins/store";

Vue.prototype.$open = link => window.open(link, '_blank');

new Vue({
    vuetify,
    router,
    store,
    render: h => h(App)
}).$mount('#app')
